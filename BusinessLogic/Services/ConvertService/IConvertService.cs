﻿using System.Drawing;
using System.Threading.Tasks;

namespace BusinessLogic.Services.ConvertService
{
    public interface IConvertService
    {
        Task Convert(string originalImage, string outputImage);
    }
}
