﻿using ImageMagick;
using ImageMagick.Formats;
using System.Threading.Tasks;

namespace BusinessLogic.Services.ConvertService
{
    public class ConvertService : IConvertService
    {
        public async Task Convert(string originalImage, string outputImage)
        {
            await Task.Run(() =>
            {   var settings = new MagickReadSettings
                {
                Defines = new DngReadDefines
                    {
                        UseCameraWhitebalance = true,
                    },
                };
                var convertImage = new MagickImage(originalImage, settings);
                convertImage.Write(outputImage);
            });
        }
    }
}
