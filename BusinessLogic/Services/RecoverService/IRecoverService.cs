﻿using System.Drawing;
using System.Threading.Tasks;

namespace BusinessLogic.Services.RecoverService
{
    public interface IRecoverService
    {
        Task Recover(string inputFile, string outputFile);
    }
}
