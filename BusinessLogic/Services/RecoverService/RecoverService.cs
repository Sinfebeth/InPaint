﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Threading.Tasks;

namespace BusinessLogic.Services.RecoverService
{
    public class RecoverService : IRecoverService
    {
        private readonly Image<Gray, byte> _maskVertical;
        private readonly Image<Gray, byte> _maskHorizontal;

        public RecoverService(Image<Gray, byte> maskVertical, Image<Gray, byte> maskHorizontal)
        {
            _maskVertical = maskVertical;
            _maskHorizontal = maskHorizontal;
        }
        public async Task Recover(string inputFile, string outputFile)
        {
            var inputImage = new Image<Bgr,byte>(inputFile);
            inputImage = ImageResize(inputImage, _maskHorizontal);
            var outputImage = new Image<Bgr, byte>(inputImage.Width, inputImage.Height);
            var mask = GetMask(inputImage.Width, inputImage.Height);
            await Task.Run(() =>
            {
                CvInvoke.Inpaint(inputImage, mask, outputImage, 3, Emgu.CV.CvEnum.InpaintType.Telea);
                CvInvoke.Imwrite(outputFile, outputImage);
            });
        }
        private Image<Gray, byte> GetMask(int width, int height)
        {
            return width > height ? _maskHorizontal : _maskVertical;
        }
        private Image<Bgr,byte> ImageResize(Image<Bgr,byte> inputFile, Image<Gray,byte> maskHorizontal)
        {
            if(inputFile.Width> inputFile.Height)
            {
                return inputFile.Resize(maskHorizontal.Width, 
                    maskHorizontal.Height, Emgu.CV.CvEnum.Inter.NearestExact);
            }
            else
            {
                return inputFile.Resize(maskHorizontal.Height,
                    maskHorizontal.Width, Emgu.CV.CvEnum.Inter.NearestExact);
            }
        }
    }
}
