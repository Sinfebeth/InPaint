﻿using BusinessLogic.Services.RecoverService;
using BusinessLogic.Services.ConvertService;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;
using System.IO;
using System.Windows.Input;

namespace Presentation
{
    public partial class MainWindow : Window
    {
        private readonly IRecoverService _recoveryService;
        private readonly IConvertService _convertService;
        public string selectedFormat { get; private set; } = String.Empty;

       public MainWindow(IRecoverService recoveryService, IConvertService convertService)
        {
            _recoveryService = recoveryService;
            _convertService = convertService;

            InitializeComponent();
        }

        private void Recover_Button_Click(object sender, RoutedEventArgs e)
        {
            RecoverDragDrop recoverDragDrop = new RecoverDragDrop(_recoveryService,_convertService);
            recoverDragDrop.Owner = this;
            this.Hide();
            recoverDragDrop.ShowDialog();
            this.Show();
        }
        
        private void Convert_Button_Click(object sender, RoutedEventArgs e)
        {
            ConvertDragDrop convertDragDrop = new ConvertDragDrop(_convertService);
            convertDragDrop.Owner = this;
            this.Hide();
            convertDragDrop.ShowDialog();
            this.Show();
        }
        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void ButtonClose_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonCollapse_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void ButtonPinOwerAllWindows_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            if (this.Topmost == true)
            {
                this.Topmost = false;
            }
            else
            {
                this.Topmost = true;
            }

        }
    }
}
