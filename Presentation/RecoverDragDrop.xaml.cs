﻿using BusinessLogic.Services.RecoverService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLogic.Services.ConvertService;

namespace Presentation
{
    /// <summary>
    /// Логика взаимодействия для RecoverDragDrop.xaml
    /// </summary>
    public partial class RecoverDragDrop : Window
    {
        private readonly IRecoverService _recoveryService;
        private readonly IConvertService _convertService;

        public RecoverDragDrop(IRecoverService recoveryService, IConvertService convertService)
        {
            InitializeComponent();
            _recoveryService = recoveryService;
            _convertService = convertService;


        }
        string[] originalImages;
        private void ListBox_Drop(object sender, System.Windows.DragEventArgs e)
        {
            TextBlockDragDrop.Opacity = 0;
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                originalImages = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                foreach (string image in originalImages)
                {
                    originalImagesList.Items.Add(image);
                }
                ArrayClear(originalImages);
            }
        }
        private void TextBlock_Drop(object sender, System.Windows.DragEventArgs e)
        {
            TextBlockDragDrop.Opacity = 0;
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                originalImages = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                foreach (string image in originalImages)
                {
                    originalImagesList.Items.Add(image);
                }
                ArrayClear(originalImages);

            }
        }
        private void originalImagesList_click(object sender, MouseButtonEventArgs e)
        {
            TextBlockDragDrop.Opacity = 0;
            var openDialog = new OpenFileDialog();
            openDialog.Multiselect = true;
            if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                originalImages = openDialog.FileNames;
                foreach (string image in originalImages)
                {
                    originalImagesList.Items.Add(image);
                }
            }
            ArrayClear(originalImages);
        }

        private async void StartRecoverButton(object sender, RoutedEventArgs e) //ь кнопка
        {

            var saveFolder = new FolderBrowserDialog();
            var outputFile = String.Empty;
            if (saveFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                RecoverProgressBar.Maximum = originalImagesList.Items.Count;

                    foreach (string image in originalImagesList.Items)
                    {
                        FileInfo fileInf = new FileInfo(image);
                        outputFile = saveFolder.SelectedPath
                                + "\\"
                                + System.IO.Path.GetFileNameWithoutExtension(image)
                                + "recover"
                                + fileInf.Extension;
                    try {                                                               //как противно
                        await _recoveryService.Recover(image, outputFile);
                        }
                    catch
                    {
                        await _convertService.Convert(image, saveFolder.SelectedPath
                            + "\\"
                            + System.IO.Path.GetFileNameWithoutExtension(image)
                            + "convert"
                            + ".jpg");
                        await _recoveryService.Recover(saveFolder.SelectedPath
                            + "\\"
                            + System.IO.Path.GetFileNameWithoutExtension(image)
                            + "convert"
                            + ".jpg", saveFolder.SelectedPath
                                + "\\"
                                + System.IO.Path.GetFileNameWithoutExtension(image)
                                + "recover" + ".jpg");
                        File.Delete(saveFolder.SelectedPath
                            + "\\"
                            + System.IO.Path.GetFileNameWithoutExtension(image)
                            + "convert"
                            + ".jpg");
                    }

                        RecoverProgressBar.Dispatcher.Invoke(() =>
                        {
                        RecoverProgressBar.Value += 1;
                            if (RecoverProgressBar.Value == RecoverProgressBar.Maximum)
                            {
                                System.Windows.MessageBox.Show("recover done");
                                RecoverProgressBar.Value = 0;
                            };
                        });
                }
                
            }

        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void ButtonClose_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonCollapse_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void ButtonPinOwerAllWindows_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            if (this.Topmost == true){
                this.Topmost = false;
            }
            else
            {
                this.Topmost = true;
            }
            
        }
        private void ArrayClear(string[] originalImages)
        {
            if (originalImages.Length != 0)
            {
                Array.Clear(originalImages, 0, originalImages.Length);
            }
        }
    }
}
