﻿using BusinessLogic.Services.RecoverService;
using BusinessLogic.Services.ConvertService;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Windows;

namespace Presentation
{
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var horizontalMask = new Image<Gray, byte>(@"D:\Mega\MEGAsync\InPaint\horizontalMask.jpg");
            var verticallMask = new Image<Gray, byte>(@"D:\Mega\MEGAsync\InPaint\verticalMask.jpg");
            var recoveryService = new RecoverService(verticallMask,horizontalMask);
            var convertService = new ConvertService();
            var mainWindow = new MainWindow(recoveryService, convertService);
            mainWindow.ShowDialog();
        }
    }
}
