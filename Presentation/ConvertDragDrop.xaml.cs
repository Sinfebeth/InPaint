﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLogic.Services.ConvertService;

namespace Presentation
{
    /// <summary>
    /// Логика взаимодействия для ConvertDragDrop.xaml
    /// </summary>
    public partial class ConvertDragDrop : Window
    {
        private readonly IConvertService _convertService;
        public string selectedFormat { get; private set; } = String.Empty;

        public ConvertDragDrop(IConvertService recoveryService)
        {
            InitializeComponent();
            _convertService = recoveryService;

        }
        string[] originalImages;
        private void ListBox_Drop(object sender, System.Windows.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                originalImages = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                foreach (string image in originalImages)
                {
                    originalImagesList.Items.Add(image);
                }
                ArrayClear(originalImages);
                TextBlockDragDrop.Opacity = 0;
            }
        }
        private void TextBlock_Drop(object sender, System.Windows.DragEventArgs e)
        {
            TextBlockDragDrop.Opacity = 0;
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                originalImages = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                foreach (string image in originalImages)
                {
                    originalImagesList.Items.Add(image);
                }
                ArrayClear(originalImages);
            }
        }
        private void originalImagesList_click(object sender, MouseButtonEventArgs e)
        {
            TextBlockDragDrop.Opacity = 0;
            var openDialog = new OpenFileDialog();
            openDialog.Multiselect = true;
            if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                originalImages = openDialog.FileNames;
                foreach (string image in originalImages)
                {
                    originalImagesList.Items.Add(image);
                }
            }
            ArrayClear(originalImages);
        }

        private async void StartConvertButton(object sender, RoutedEventArgs e)
        {
            var saveFolder = new FolderBrowserDialog();
            var outputFile = String.Empty;
            var radioButtonsFormat = new System.Windows.Controls.RadioButton();
            if (saveFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                RecoverProgressBar.Maximum = originalImagesList.Items.Count;
                foreach (string image in originalImagesList.Items)
                {
                    outputFile = saveFolder.SelectedPath
                            + "\\"
                            + System.IO.Path.GetFileNameWithoutExtension(image)
                            + "convert"
                            + selectedFormat;
                    await _convertService.Convert(image, outputFile);
                    RecoverProgressBar.Dispatcher.Invoke(() =>
                    {
                        RecoverProgressBar.Value += 1;
                        if (RecoverProgressBar.Value == RecoverProgressBar.Maximum)
                        {
                            System.Windows.MessageBox.Show("convert done");
                            RecoverProgressBar.Value = 0;
                        };
                    });
                }
            }
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void ButtonClose_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonCollapse_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void ButtonPinOwerAllWindows_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            if (this.Topmost == true)
            {
                this.Topmost = false;
            }
            else
            {
                this.Topmost = true;
            }

        }

        private void SelectFormat(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.RadioButton item)
            {
                selectedFormat= item.Content.ToString();
            }
        }
        private void ArrayClear(string[] originalImages)
        {
            if (originalImages.Length != 0)
            {
                Array.Clear(originalImages, 0, originalImages.Length);
            }
        }
    }
}
